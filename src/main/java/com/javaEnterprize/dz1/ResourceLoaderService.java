package com.javaEnterprize.dz1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

@Component("question")
public class ResourceLoaderService implements ResourceLoaderAware {

    private ResourceLoader resourceLoader;

    public ResourceLoaderService(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }

    public String[] getResourceDataUsingFilePath(String path) throws IOException {
        Resource resource = resourceLoader.getResource(path);
        InputStream in = resource.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        String[] lines = new String[5];
        for (int i = 0; i < lines.length; i++) {
            String line = reader.readLine();
            if (line == null)
                break;
            lines[i] = (line.split(":"))[1];
        }
        reader.close();
        return lines;
    }

    @Override
    public void setResourceLoader(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }


}
