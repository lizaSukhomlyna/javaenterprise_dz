package com.javaEnterprize.dz1;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

@Component("menu")
public class MenuService {

    Scanner sc = new Scanner(System.in);
    @Value("${rigth.answer.path}")
    private String rightAnswearPath;
    @Value("${menu.path}")
    private String path;

    @Value("${menu.language}")
    private String language;

    @Value("${result.answer.path}")
    private String resultPath;


    public void callMenu(ApplicationContext context) throws IOException {
        ResourceLoaderService question = (ResourceLoaderService) context.getBean("question");
        String[] questions = question.getResourceDataUsingFilePath(path);
        String[] rightAnswears = question.getResourceDataUsingFilePath(rightAnswearPath);
        System.out.println(Arrays.toString(questions));
        int countRightAnswears = 0;
        System.out.println(Arrays.toString(rightAnswears));
        for (int i = 0; i < questions.length; i++) {
            System.out.println(questions[i]);
            String input = " " + sc.nextLine();
            if (input.equals(rightAnswears[i])) {
                countRightAnswears++;
            }

        }
        String[] resultAnsw = question.getResourceDataUsingFilePath(resultPath);
        if (countRightAnswears > 2) {
            System.out.println(resultAnsw[0]);
        } else {
            System.out.println(resultAnsw[1]);
        }
    }


}
